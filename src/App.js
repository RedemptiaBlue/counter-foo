import { render } from "@testing-library/react";
import React, { Component } from "react";

/**
 * If count is 0 display "POOP_TEXT"
 * If count is divisible by 3 display "COUNTER_TEXT"
 * If count is divisible by 5 display "FOO_TEXT"
 * If count is divisible by 3 && 5 display "COUNTER_FOO_TEXT"
 * If count is not divisible by 3, 5 display "POOP_TEXT"
 * The increment button should increase the "count" variable
 * The decrement button should decrease the "count" variable
 * The "count" variable should never go below 0
 */

const COUNTER_TEXT = "Counter";
const FOO_TEXT = "Foo";
const COUNTER_FOO_TEXT = "🙅🏿‍♂️ Counter Foo 🙅🏿‍♂️";
const POOP_TEXT = "💩";

class App extends Component {
  state = {
    count: 0,
    text: POOP_TEXT,
  };

  increment = (event) => {
    this.setState((state) => ({
      count: state.count + 1,
      text: this.changeTextByDivisibility(state, 1),
    }));
  };

  decrement = (event) => {
    if (this.state.count > 0) {
      this.setState((state) => ({
        count: state.count - 1,
        text: this.changeTextByDivisibility(state, -1),
      }));
    }
  };

  changeTextByDivisibility = (state, x) => {
    if ((this.state.count + x) % 3 === 0 && (this.state.count + x) % 5 === 0) {
      return COUNTER_FOO_TEXT;
    } else if ((this.state.count + x) % 3 === 0) {
      return COUNTER_TEXT;
    } else if ((this.state.count + x) % 5 === 0) {
      return FOO_TEXT;
    } else {
      return POOP_TEXT;
    }
  };

  render() {
    const count = this.state.count;
    const text = this.state.text;
    return (
      <div style={styles.app}>
        <h1>{count}</h1>
        <h3>{text}</h3>
        <div style={styles.buttons}>
          <button onClick={this.increment}>Increment</button>
          <button onClick={this.decrement}>Decrement</button>
        </div>
      </div>
    );
  }
}

const styles = {
  app: {
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  buttons: {
    display: "flex",
    flexDirection: "row",
  },
};
export default App;
